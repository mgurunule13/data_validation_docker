# -*- coding: utf-8 -*-
"""
Created on Thu Nov 11 18:51:17 2021

@author: 30061160
"""
import datetime
import glob
import os
import pickle

import numpy as np
import pandas as pd
from flask import Flask, jsonify, request

from data_pre_process import data_clean
import sys


def log_writer(logfile, record_content):
    """ Write log file """
    with open(logfile, 'a', encoding="utf8") as file_handler:
        current_time = datetime.datetime.now()
        file_handler.write(str(current_time) + " " + record_content + "\n")


app = Flask(__name__)


@app.route("/", methods=['GET'])
def healthcheck():
    """ Health check """
    return "OK"


def find_bmu(self_organizing_maps, x_weight):
    """ Find the best matching unit for the given data point """
    distance_squared = (np.square(self_organizing_maps - x_weight)).sum(axis=2)  # the distance between the data point and the weight
    return np.unravel_index(np.argmin(distance_squared, axis=None), distance_squared.shape)  # the index of the minimum distance


@app.route('/', methods=['POST'])
def test():
    if not os.path.exists(os.path.dirname(os.path.realpath(__file__))+'/logs'):
        os.makedirs(os.path.dirname(os.path.realpath(__file__))+'/logs')
    """ Test API """ 
    log_file = (os.path.dirname(os.path.realpath(__file__)) + "/logs/log.txt")  # log file
    if os.path.exists(log_file):  # if log file exists, delete it
        os.remove(log_file)  # delete log file
    log_writer(log_file, "API is running")  # write log
    record_content = "Received a request"
    log_writer(log_file, record_content)
    model_path = './model/'  # model path
    error_flag = False  # error flag
    try:
        for model_file in os.listdir(model_path):  # check if model exists
            if model_file.endswith('.sav'):
                all_files = glob.glob(model_path + model_file)  # get all files in model path
        record_content = "Loading the KMeans model"
        log_writer(log_file, record_content)
        model_name = max(all_files, key=os.path.getctime)  # get the latest model
    except:
        error_flag = True
        record_content = "Error in loading the model"
        log_writer(log_file, record_content)

    if not error_flag:
        try:
            with open(model_name, "rb") as model_file:
                model = pickle.load(model_file)  # load the model
            for weights_file in os.listdir(model_path):
                if weights_file.endswith('.npy'):  # check if weights exists
                    all_files = glob.glob(model_path + weights_file)  # get all files in model path
            som_weights = max(all_files, key=os.path.getctime)  # get the latest weights
            record_content = "Loading the weights"
            log_writer(log_file, record_content)
            clust = np.load(som_weights)  # load weights
        except:
            error_flag = True
            record_content = "Error in loading the weights"
            log_writer(log_file, record_content)  # log error
    if not error_flag:
        test_data = request.get_json()  # get the test data
        test_dataframe = pd.DataFrame.from_dict(test_data, orient='columns')  # convert to dataframe
        try:
            test_dataframe = data_clean(test_dataframe)  # clean the data
            if isinstance(test_dataframe, pd.DataFrame):
                if 'timestamp' in test_dataframe.columns:
                    test_dataframe.drop('timestamp', axis=1, inplace=True)
                column_list = [col.split('/')[-1] for col in test_dataframe.columns]
                final_wms_list = ['EXPORT_ACTIVE_ENERGY', 'RADIATION_GHI', 'RADIATION_GII'] #This will be taken from config file
                if len(column_list) != len(final_wms_list):
                    error_flag = True
                    record_content = "Error in the number of columns"
                    log_writer(log_file, record_content)
                else:
                    for col1 in column_list:
                        if col1 not in final_wms_list:  # check if the columns are correct
                            error_flag = True
                            record_content = "Error in the columns"
                            log_writer(log_file, record_content)
            else:
                error_flag = True
                record_content = "Error in the preprocessing the dataframe, not a dataframe"
                log_writer(log_file, record_content)
        except:
            error_flag = True
            record_content = "Error in the reading the file"
            log_writer(log_file, record_content)
    if not error_flag:
        try:
            highlight_missing = ~np.isfinite(test_dataframe)  # find missing values
            mean_of_data = np.nanmean(test_dataframe, 0, keepdims=1)  # get the mean of data
            modified_test_data = np.where(highlight_missing, mean_of_data, test_dataframe)  # replace missing values with mean
            label = model.predict(modified_test_data)  # predict the label
            for i in enumerate(label):
                cluster_labels = label[i]  # get the cluster labels
                coordinates = find_bmu(clust[cluster_labels],
                                modified_test_data[i].reshape(1, test_dataframe.shape[1]))  # find the bmu
                coordinate_a = coordinates[0]  # coordinates of the first cluster
                coordinate_b = coordinates[1]  # coordinates of the second cluster
                for j in range(test_dataframe.shape[1]):  # get the coordinates
                    if highlight_missing.values[i][j] is True:  # if missing value
                        vector = clust[cluster_labels][coordinate_a][coordinate_b]  # get the vector
                        modified_test_data[i][j] = vector[j]  # replace the missing value with the vector
            final_dataframe = pd.DataFrame(modified_test_data, columns=test_dataframe.columns)
            for column in final_dataframe.columns:
                final_dataframe[column] = final_dataframe[column] * test_dataframe[column].abs().max()  # normalize
            result = final_dataframe.to_json()
            result = jsonify(result)  # convert to json
            return result
        except:
            error_flag = True
    if error_flag:
        return jsonify(test_dataframe.to_json())
    return 


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8300, debug=True)
