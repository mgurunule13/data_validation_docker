
import os

import pickle
import sys
import datetime
import warnings
import glob
import json
import yaml
import numpy as np
import pandas as pd
warnings.filterwarnings("ignore")
from google.cloud import storage

from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt

# Function to load yaml configuration file
def load_config(config_name):
    ''' loding config file '''
    CONFIG_PATH = "./"
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)
    return config

def list_blobs_with_prefix(bucket_name, prefix, delimiter=None):
    """Lists all the blobs in the bucket that begin with the prefix.
    """  
    storage_client = storage.Client()    
    blobs = storage_client.list_blobs(bucket_name, prefix=prefix, delimiter=delimiter)
    print("blobs ",blobs)  
    i = 0
    for blob in blobs:
        if blob.size != 0:  # do not download empty file
            i = i+1
            final_dest = download_path + str(i) +".json"
            blob.download_to_filename(final_dest)
    if delimiter:
        for prefix in blobs.prefixes:
            continue


def upload_blob(bucket_name):
    '''Uploads a file to the bucket.
    # The ID of your GCS bucket
    # bucket_name = "your-bucket-name"
    # The path to your file to upload
    # source_file_name = "local/path/to/file"
    # The ID of your GCS object
    # destination_blob_name = "storage-object-name"  '''
    
    ##Update below code for appending imputed dataframes/data before saving into GCS
    dir_list = os.listdir(upload_path)
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    for file_name in dir_list:
        destination_blob_name =  config['gcp_output_folder'] + file_name
        blob = bucket.blob(destination_blob_name)
        source_file_name = upload_path + file_name
        blob.upload_from_filename(source_file_name)
        
def log_writer(logfile,VAR_CONTENT):
    '''
    Parameters
    ----------
    logfile : TYPE  - log file object
        DESCRIPTION -  file object.
    VAR_CONTENT : string
        DESCRIPTION -  message to be written.
    Returns
    -------
    None.
    '''
    with open(logfile,'a') as file_handler:
        current_time = datetime.datetime.now()
        file_handler.write(str(current_time)+ " " + VAR_CONTENT + "\n")
def data_clean(df):
    global VAR_CONTENT
    '''
    Parameters
    ----------
    df : TYPE - dataframe
        DESCRIPTION - pre- procssing the input data.
    Returns
    -------
    new_df : TYPE - dataframe
        DESCRIPTION - pre-processed dataframe.
    '''
    ##
    col_upper_case = [col.upper() for col in df.columns]
    df.columns     = col_upper_case
    
    VAR_CONTENT = "THE COLUMNS 'SOURCE_ID_NEURON' OR 'VALUE_FLOAT'OR'TIMESTAMP' ARE NOT PRESENT IN THE DATA"
   
    df = df.loc[:,config['filter_columns']]
    VAR_CONTENT = " 'TIMESTAMP' ARE NOT PRESENT IN THE DATA"
    df["ts"] = pd.to_datetime(df["TIMESTAMP"], format='%Y-%m-%d %H:%M:%S %Z', errors='coerce')
    mask = df.ts.isnull()
    if mask.sum():
        df.loc[mask, "ts"] = pd.to_datetime(df[mask]["TIMESTAMP"], \
                                            format='%Y-%m-%d %H:%M:%S.%f %Z',errors='coerce')
        df['TIMESTAMP']=df['ts']
        df.drop(columns={'ts'},inplace=True)
    else:
        df["TIMESTAMP"] = pd.to_datetime(df["TIMESTAMP"])
    
    #VAR_CONTENT = "THE COLUMNS PLANT,RNOC_TAG_NAME,VALUE_TYPE,QUALITY ARE NOT PRESENT IN THE DATA"

    new_df = df.pivot_table(index = "TIMESTAMP",columns="SOURCE_ID_NEURON",\
                            values = "VALUE_FLOAT",dropna=False)
    new_df = new_df.reset_index()
    
    #Might not need below code in future need to discuss
    col_upper_case = [col.upper() for col in new_df.columns]
    new_df.columns = col_upper_case
    
    
    VAR_CONTENT = "CONVERTED THE TABLE TO HAVE MULTIPLE COLUMNS"
    log_writer(log_file,VAR_CONTENT)
    
    if new_df['TIMESTAMP'].dtypes=='datetime64[ns, UTC]':
        new_df.set_index(new_df['TIMESTAMP'],inplace=True)
        new_df = new_df.tz_convert('Asia/Kolkata')
        #new_df.drop(columns = "TIMESTAMP",inplace=True)
    return new_df

def find_BMU(som_array,x):
    '''
    Parameters
    ----------
    som_array : TYPE - numpy array
        DESCRIPTION - numpy array having SOM values.
    x : TYPE - numpy array
        DESCRIPTION - input to som.
    Returns
    -------
    TYPE - np_array
        DESCRIPTION - besy matching unit.
    '''
    distSq = (np.square(som_array - x)).sum(axis=2)
    return np.unravel_index(np.argmin(distSq, axis=None), distSq.shape)

def update_weights(som_array, train_ex, learn_rate, radius_sq,
                   BMU_coord, step=3):
    ''' update the weights for Som '''
    g, h = BMU_coord
    #if radius is close to zero then only BMU is changed
    if radius_sq < 1e-3:
        som_array[g,h,:] += learn_rate * (train_ex - som_array[g,h,:])
        return som_array
    for i in range(max(0, g-step),min(som_array.shape[0], g+step)):
        for j in range(max(0, h-step),min(som_array.shape[1], h+step)):
            x_dis=np.square(i - g)
            y_dis=np.square(j - h)
            dist_sq = x_dis + y_dis
            dist_func = np.exp(-dist_sq / 2 / radius_sq)
            som_array[i,j,:] += learn_rate * dist_func * (train_ex - som_array[i,j,:])
    return som_array

def train_SOM(som_array, train_data, learn_rate = .1, radius_sq = 1,
             lr_decay = .01, radius_decay = .01, epochs = 10):
    ''' train som model '''
    learn_rate_0 = learn_rate
    radius_0 = radius_sq
    for epoch in np.arange(0, epochs):
        rand.shuffle(train_data)
        for train_ex in train_data:
            g, h = find_BMU(som_array, train_ex)
            som_array = update_weights(som_array, train_ex,
                                 learn_rate, radius_sq, (g,h))
        learn_rate = learn_rate_0 * np.exp(-epoch * lr_decay)
        radius_sq = radius_0 * np.exp(-epoch * radius_decay)
    return som_array

def train_SOM_cluster(train_set1,var_lr):
    '''   train som cluster '''
    clust = []
    for n in range(3):
        som_array = np.random.uniform(low=0.0, high=1.0, size=(25,25,len(train_set1.columns)-1))
        a=train_set1.loc[train_set1['Group'] == n]
        a.drop('Group',axis=1,inplace=True)
        som_array = train_SOM(som_array, np.array(a),var_lr, epochs=50)
        clust.append(som_array)
    return clust

def test_model(clust,model,test_df,var_ins):
    ''' test the model '''
    X_test = test_df.copy()
    df = test_df.copy()
    X_test.reset_index('TIMESTAMP',inplace=True)
    df.reset_index('TIMESTAMP',inplace=True)
    df.drop('TIMESTAMP',axis=1,inplace=True)
    X_test.drop('TIMESTAMP',axis=1,inplace=True)
    test=X_test.copy()
    train_set1 = df.copy()
    ### columns logic
    if var_ins == 'inverter':
        testing_columns = config['inverter_test_columns']
    elif var_ins == 'wms':
        testing_columns = config['wms_test_columns']
    else:
        testing_columns = config['transformer_test_columns']
    index =0
    for column in test.columns:
        test[column] = test[column] / train_set1[column].abs().max()
        
        # for col_endwith in testing_columns:
        #     for col in test.columns:
        #         if col.endswith(col_endwith):
        #             test[col].iloc[index*40:(index*40+20)] = np.nan
        #             index +=1
    for col_endwith in testing_columns:
         for col in test.columns:
             if col.endswith(col_endwith):
                 test[col].iloc[index:index+round(len(test)/len(testing_columns))] = np.nan
                 index=index+round(len(test)/len(testing_columns))
                    
    missing = ~np.isfinite(test)
    var_mu = np.nanmean(test, 0, keepdims=1)
    Test10 = np.where(missing, var_mu, test)
    label = model.predict(Test10)
    for i in range(len(label)):
        c=label[i]
        coor=find_BMU(clust[c],Test10[i].reshape(1,X_test.shape[1]))
        a=coor[0]
        b=coor[1]
        for j in range(test.shape[1]):
            if missing.values[i][j]==True:
                vector=clust[c][a][b]
                Test10[i][j] = vector[j]
    Final= pd.DataFrame(Test10, columns =df.columns)
    for column in Final.columns:
        Final[column] = Final[column] * train_set1[column].abs().max()
    Root_Mean_Squared_Error = 0
    for col_endwith in testing_columns:
        for col in Final.columns:
            if col.endswith(col_endwith):
                Root_Mean_Squared_Error = Root_Mean_Squared_Error +\
                    np.sqrt(mean_squared_error(Final[col], X_test[col]))
    return Final,X_test,missing,(Root_Mean_Squared_Error / len(testing_columns))

def Plot(Final,X_test,missing,var_ins):
    '''  plot the graph '''
    # get the list from config file based on instrument
    if var_ins == 'inverter':
        testing_columns = config['inverter_test_columns']
    elif var_ins == 'wms':
        testing_columns = config['wms_test_columns']
    else:
        testing_columns = config['transformer_test_columns']
    for col_endwith in testing_columns:
        for col in Final.columns:
            if col.endswith(col_endwith):
                i=list(Final[col])
                r=list(X_test[col])
                m=list(missing[col])
                miss=[]
                for j in range(len(i)):
                    if m[j]:
                        miss.append(0)
                    else:
                        miss.append(r[j])
                feat_df=pd.DataFrame(pd.DataFrame({"missing":miss,'imputed':i,"real":r}))
                feat_df.iloc[0:300].plot(subplots=False,title=col)
                
def identify_instrument(columns):
    ''' identify the instrument '''
    instrument = ""
    column_list = [col.split('/')[-1] for col in columns]
    wms_col      = [col.split('/')[-1] for col in config['wms_test_columns']]
    tranfo_col   = [col.split('/')[-1] for col in config['transformer_test_columns']]
    inverter_col = [col.split('/')[-1] for col in config['inverter_test_columns']]
    if wms_col[0] in column_list :
        instrument = 'wms'
    elif tranfo_col[0] in column_list:
        instrument = 'transformer'
    elif inverter_col[0] in column_list:
        instrument = 'inverter'
    return instrument

class ValidationError(Exception):
    pass

## Logger file
def main(log_file):

    ## Load config file
    global VAR_CONTENT 
    # determine local download and upload path folder
    print('Inside main')
    glob_file = sorted(glob.glob(download_path+ '*.json'))
    if glob_file:
        data = pd.concat(( pd.read_json(globs,lines=True) for globs in glob_file),ignore_index=True)
        if len(data) == 0 :
            VAR_CONTENT = "No records in the input dataset"
            log_writer(log_file,VAR_CONTENT)
            return 1
            #raise ValueError("No records in the input dataset")
    else:
        VAR_CONTENT = "NO FILE IN THE INPUT FOLDER"
        log_writer(log_file,VAR_CONTENT)
        return 1
        #raise ValueError("No file in the input folder")
    data_set1 = data.copy()
    VAR_CONTENT = "DATA COPIED TO A NEW VARIABLE DATA_SET1"
    log_writer(log_file,VAR_CONTENT)
    
  #  try:
    pre_processed_df=data_clean(data_set1)
    if not isinstance(pre_processed_df, pd.DataFrame):
        VAR_CONTENT = "NOT A DATAFRAME"
        log_writer(log_file,VAR_CONTENT)
        return 1
        
#        raise ValidationError(" Did not returned Dataframe ")
    VAR_CONTENT = "PIVOTING DONE"
    log_writer(log_file,VAR_CONTENT)

    if 'TIMESTAMP' in pre_processed_df.columns:
        pre_processed_df.drop(['TIMESTAMP'],axis = 1,inplace=True)
    final_df = pre_processed_df.interpolate(method='time')
    final_df = final_df.resample('5min').mean()
    final_df = final_df.drop_duplicates()
    final_df = final_df.between_time('6:30', '18:30')
    final_df.dropna(inplace=True)
    VAR_CONTENT = "PRE-PROCESSING AFTER PIVOTING DONE"
    log_writer(log_file,VAR_CONTENT)
    var_ins = identify_instrument(final_df.columns)
    if var_ins in ('transformer','inverter'):
        for col in final_df.columns:
            if var_ins == 'inverter':
                if col.endswith('DC_CAPACITY') or\
                    col.endswith('INV_STS_CODE') or col.endswith('TOTAL_ENERGY'):
                    final_df.drop(columns = {col},inplace=True)
    ## checl the list from config file
            if col.endswith('REACTIVE_POWER') or col.endswith('IMPORT_ACTIVE_ENERGY'):
                  if len(final_df[col].unique())==1:
                        if final_df[col].unique()==0: #i.e it has only zero value
                            final_df.drop(columns = {col},inplace=True)
    if var_ins == 'wms':
            column_list = [col.split('/')[-1] for col in final_df.columns]
            if len(column_list) != len(config['wms_columns']):
                VAR_CONTENT = "COLUMN NUMBER DID NOT MACTHED FOR WMS"
                log_writer(log_file,VAR_CONTENT)
                return 1
        #        raise ValidationError(" Column number did not macth for WMS ")
            else:
                for col1 in column_list:
                    if col1 not in config['wms_columns']:
                        VAR_CONTENT = "COLUMN NOT MATCHED"
                        log_writer(log_file,VAR_CONTENT)
                        return 1
        #                raise ValidationError("column not matched")
                for col1 in final_df.columns:
                    if len(final_df[col1].unique())==1:
                        if final_df[col1].unique()==0:
                            VAR_CONTENT = "INVALID DATA FOR WMS IN " + str(col1)
                            log_writer(log_file,VAR_CONTENT)
                            return 1
        #                    raise ValidationError("Invalid data for WMS")
                            #i.e it has only zero value
    
    test_df    = final_df.iloc[int(len(final_df) * .7):]
    train_set1 = final_df.iloc[0:int(len(final_df) * .7)]
    train_set1.reset_index('TIMESTAMP',inplace=True)
    train_set1.drop('TIMESTAMP',axis=1,inplace=True)

    for typ in train_set1.dtypes:
        if typ not in ['float64','int64']:
            VAR_CONTENT = "Only Numeric values allowed" 
            log_writer(log_file,VAR_CONTENT)
            return 1

    for column in train_set1.columns:
        train_set1[column] = train_set1[column] / train_set1[column].abs().max()
    km = KMeans(n_clusters=3, random_state=0, n_init=30)
    km.fit(train_set1)
    label = km.predict(train_set1)
    train_set1['Group'] = label
    train_set1['Group'].value_counts()
    if var_ins == 'inverter':
        sav_filename = config['inverter_sav_File']
    elif var_ins == 'wms':
        sav_filename = config['wms_sav_file']
    else :
        sav_filename = config['trafo_sav_File']
    with open(sav_filename,'wb') as f:
        pickle.dump(km, f)
        f.close()
    VAR_CONTENT = "PICKLE FILE SAVED"
    log_writer(log_file,VAR_CONTENT)
    learning_rate = config['learning_rates']
    rmse_list     = []
    cluster = []
    print('Before lr starts')
    for var_lr in learning_rate:
        print('looping lr')
        clust = []
        clust = train_SOM_cluster(train_set1,var_lr)
        Final,X_test,missing,rmse = test_model(clust,km,test_df,var_ins)
        rmse_list.append(rmse)
    final_lr = learning_rate[rmse_list.index(min(rmse_list))]
    print('Got final lr')
    VAR_CONTENT = "FINDING FINAL LEARNNING RATE FOR GIVEN RMSE LIST"
    log_writer(log_file,VAR_CONTENT)
    VAR_CONTENT = "SAVING MODEL FILES FOR BEST LEARNING RATE FOUND"
    log_writer(log_file,VAR_CONTENT)
    clust = train_SOM_cluster(train_set1,final_lr)
    if var_ins == 'inverter':
        npy_filename = config['inverter_npy_File']
    elif var_ins == 'wms':
        npy_filename = config['wms_npy_file']
    else :
        npy_filename = config['trafo_npy_File']
    np.save(npy_filename,np.array(clust))
    VAR_CONTENT = "numpy FILE SAVED"
    log_writer(log_file,VAR_CONTENT)
    Plot(Final,X_test,missing,var_ins)
    
if __name__ == "__main__":
    
    #try:
    log_file = os.path.join(os.path.dirname(__file__), 'log.txt')
    
    if os.path.exists(log_file):
        os.remove(log_file)
    log_writer(log_file,"LogFile created")
    
    VAR_CONTENT = "CONFIG FILE READ FAIL" #If config fie fails below then this error must be captured.
    config = load_config("config.yaml")
    VAR_CONTENT = "CONFIG FILE READ SUCCESS"
    log_writer(log_file,VAR_CONTENT)
        
    rand = np.random.RandomState(0)
    cwd = os.getcwd()
    download_path = str(cwd) + config['download_folder']
    upload_path = str(cwd)   + config['upload_folder']
    
    # downloading input file from GCS bucket
    VAR_CONTENT = "FAIL TO READ FROM GCS, SINCE NO BUCKET NAME MATCHED or NO PATH FOUND."
    #list_blobs_with_prefix(config['bucket_name'], config['prefix'])
    VAR_CONTENT = "DATA DOWNLOADED FROM GCS BUCKET"
    log_writer(log_file,VAR_CONTENT)
    dir_list = os.listdir(upload_path)
    
    #Calling main function
    main(log_file)
    #except Exception as argument:
    #    log_writer(log_file,VAR_CONTENT)
        